<?php

namespace App\Tests;

use DateTimeImmutable;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Source\RoomBlocking\Domain\TimeInterval;
use Source\Shared\Domain\Minutes;

class TimeIntervalTest extends TestCase
{
    // todo dopisać test, który konkretnie sprawdzi czy działa parametr GAP

    /**
     * @dataProvider intervalData
     */
    public function testTimeIntervalsOverlap($starts, $ends, $newStarts, $newEnds, $expectedResult): void
    {
        $blockadeDto = TimeInterval::of(new DateTimeImmutable($starts), new DateTimeImmutable($ends));
        $timeInterval = TimeInterval::of(new DateTimeImmutable($newStarts), new DateTimeImmutable($newEnds));

        $result = $blockadeDto->timeIntervalsOverlapWithGap($timeInterval, Minutes::ofInt(0));

        self::assertEquals($expectedResult, $result);
    }

    public function intervalData(): array
    {
        return [
            ['2023-01-01', '2023-01-31', '2023-02-01', '2023-02-20', true],
            ['2023-01-01', '2023-01-31', '2023-01-05', '2023-02-05', false],
        ];
    }

    /**
     * @dataProvider invalidData
     */
    public function testShouldThrowIfEndingDateIsLowerThanStartingDate($starts, $ends): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Ending date has to be greater than starting date');

        TimeInterval::of(new DateTimeImmutable($starts), new DateTimeImmutable($ends));
    }

    public function invalidData(): array
    {
        return [
            ['2023-01-01', '2023-01-01'],
        ];
    }
}