<?php

namespace App\Tests;

use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use Ramsey\Collection\Collection;
use Source\RoomBlocking\Domain\Blockades;
use Source\RoomBlocking\Domain\Room;
use Source\RoomBlocking\Domain\RoomBlockade;
use Source\RoomBlocking\Domain\TimeInterval;
use Source\Shared\Domain\Minutes;
use Symfony\Component\Uid\Uuid;

class RoomTest extends TestCase
{
    public function testShouldCreateNewBlockadeForRoom(): void
    {
        //given
        $roomId = Uuid::v4();

        $newBlockadeStartTime = new DateTimeImmutable('2023-01-04 00:00:00');
        $newBlockadeEndTime = new DateTimeImmutable('2023-01-04 00:30:00');
        $expectedTimeInterval = TimeInterval::of($newBlockadeStartTime, $newBlockadeEndTime);
        $expectedBlockade =  new RoomBlockade($expectedTimeInterval, $roomId);


        $existingBlockadeStartTime = new DateTimeImmutable('2023-02-01');
        $existingBlockadeEndTime = new DateTimeImmutable('2023-02-03');

        $serviceTime = Minutes::ofInt(30);
        $blockades = new Collection(TimeInterval::class, [
            TimeInterval::of($existingBlockadeStartTime, $existingBlockadeEndTime)
        ]);

        $room = Room::of($roomId, $serviceTime, new Blockades($blockades));

        //when
        $newBlockade = $room->newBlockade($newBlockadeStartTime, Minutes::ofInt(30));

        //then
        self::assertEquals($newBlockade->roomId, $expectedBlockade->roomId);
        self::assertEquals($newBlockade->timeInterval, $expectedBlockade->timeInterval);
    }

    private function testShouldNotCreateNewBlockadeForRoomCauseServiceTimeIsNotOver(): void
    {
        // todo
    }

    private function testShouldNotCreateNewBlockadeForRoomCauseOtherBlockadeIsAtThisTime(): void
    {
        // todo
    }
}