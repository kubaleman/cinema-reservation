<?php

namespace Source\Shared\Domain;

use DateInterval;

class Minutes
{
    private function __construct(
        private readonly int $minutes
    ) {
    }

    public static function ofInt($minutes): static
    {
        return new self(max($minutes, 0));
    }

    public function asTimeIntervalInMinutes(): DateInterval
    {
        return new DateInterval('PT'.$this->minutes.'M');
    }
}