<?php

namespace Source\Reservation\Application;

use Source\Reservation\Domain\OverbookingNotAllowedException;
use Source\Reservation\Domain\ReservationRepository;
use Source\Reservation\Domain\RoomCapacityProvider;
use Source\Reservation\Domain\RoomSpace;
use Symfony\Component\Uid\Uuid;

class ReservationService
{
    public function __construct(
        private readonly RoomCapacityProvider $capacityProvider,
        private readonly ReservationRepository $reservationRepository
    ) {
    }

    public function makeReservation(Uuid $roomBlockadeId, int $seatsAmounts): Uuid
    {
        $roomCapacity = $this->capacityProvider->findByRoomBlockadeId($roomBlockadeId);
        $roomSpace = RoomSpace::of($roomCapacity->capacity); // todo dorobić pobieranie już zarezerwowanych miejsc bo aktualnie zawsze startujemy od 0

        try {
            $reservedSeats = $roomSpace->reserveSeat($seatsAmounts);
            $reservation = Reservation::of($roomBlockadeId, $reservedSeats);

            $this->reservationRepository->save($reservation);

            return $reservation->reservationId;

        } catch (OverbookingNotAllowedException $notAllowedException) {
            // todo
        }
    }
}