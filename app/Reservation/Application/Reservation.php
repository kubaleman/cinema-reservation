<?php

namespace Source\Reservation\Application;

use Symfony\Component\Uid\Uuid;

class Reservation
{
    private function __construct(
        public readonly Uuid $reservationId,
        public readonly Uuid $roomBlockadeId,
        public readonly array $seats
    ) {
    }

    public static function of(Uuid $roomBlockadeId, array $seats): static
    {
        return new self(Uuid::v4(), $roomBlockadeId, $seats);
    }
}