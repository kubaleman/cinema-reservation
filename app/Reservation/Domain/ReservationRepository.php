<?php

namespace Source\Reservation\Domain;

use Source\Reservation\Application\Reservation;

interface ReservationRepository
{
    public function save(Reservation $reservation);
}