<?php

namespace Source\Reservation\Domain;

use Symfony\Component\Uid\Uuid;

interface RoomCapacityProvider
{
    public function findByRoomBlockadeId(Uuid $screeningId): RoomCapacityDto;
}