<?php

namespace Source\Reservation\Domain;

use Symfony\Component\Uid\Uuid;

class RoomCapacityDto
{
    public function __construct(
        public readonly Uuid $roomId,
        public readonly int $capacity
    ) {
    }
}