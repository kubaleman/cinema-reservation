<?php

namespace Source\Reservation\Domain;

// todo tests
// refaktoru wymaga również sam model miejsc, aktualnie jest to po prostu lista, które nie pozwala nawet wybrać miejsca
class RoomSpace
{
    private function __construct(
        private readonly int $roomCapacity,
        private int $reservedSeatsAmount
    ) {
    }

    public static function of(int $roomCapacity, int $reservedSeatsAmount = 0): static
    {
        return new self($roomCapacity, $reservedSeatsAmount);
    }

    public function reserveSeat(int $amount): array
    {
        if ($this->roomCapacity < $this->reservedSeatsAmount += $amount) {
            throw new OverbookingNotAllowedException();
        }

        $this->reservedSeatsAmount += $amount;
        $reservedSeats = [];

        for ($i = $this->reservedSeatsAmount ; $i > $this->reservedSeatsAmount - $amount ; $i--) {
            $reservedSeats[] = $i;
        }

        return $reservedSeats;
    }
}