<?php

namespace Source\RoomBlocking\Domain;

use Ramsey\Collection\Collection;
use Source\Shared\Domain\Minutes;

class Blockades
{

    /**
     * @param TimeInterval[] $timeIntervals
     */
    public function __construct(
        public readonly Collection $timeIntervals
    ) {
    }

    public function putBlockade(TimeInterval $blockadeInterval, Minutes $timeGapBetweenBlockades)
    {
        foreach ($this->timeIntervals as $existingBlockade) {
            if (!$existingBlockade->timeIntervalsOverlapWithGap($blockadeInterval, $timeGapBetweenBlockades)) {
                throw new CannotAddBlockadeException("Cannot add new blockade");
            }
        }

        $this->timeIntervals->add($blockadeInterval);
    }
}