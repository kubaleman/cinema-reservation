<?php

namespace Source\RoomBlocking\Domain;

use Symfony\Component\Uid\Uuid;

class RoomBlockade // todo możliwe, że znalazłby się lepsza nazwa dla tego obiektu
{
    public readonly Uuid $roomBlockadeId;

    public function __construct(
        public readonly TimeInterval $timeInterval,
        public readonly Uuid $roomId,
    ) {
        $this->roomBlockadeId = Uuid::v4();
    }
}