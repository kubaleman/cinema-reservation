<?php

namespace Source\RoomBlocking\Domain;

use DateTimeImmutable;
use InvalidArgumentException;
use Source\Shared\Domain\Minutes;

class TimeInterval
{
    // possibly Embedded
    public function __construct(
        private readonly DateTimeImmutable $starts,
        private readonly DateTimeImmutable $ends,
    ) {
    }

    public static function of(DateTimeImmutable $starts, DateTimeImmutable $ends): static
    {
        if ($starts >= $ends) {
            throw new InvalidArgumentException("Ending date has to be greater than starting date");
        }

        return new self($starts, $ends);
    }

    // metoda nieużyta, pokazując możliwości wielu zachowań zhermetyzowanych w jednym obiekcie
    public function timeIntervalsOverlap(TimeInterval $timeInterval): bool
    {
        return $timeInterval->ends <= $this->starts || $timeInterval->starts >= $this->ends;
    }

    public function timeIntervalsOverlapWithGap(TimeInterval $timeInterval, Minutes $gapForComparing): bool
    {
        $gapDateInterval = $gapForComparing->asTimeIntervalInMinutes();
        $blockadeEndsWithService = $timeInterval->ends->add($gapDateInterval);
        $blockadeStartsBeforeService = $timeInterval->starts->sub($gapDateInterval);

        return $blockadeEndsWithService <= $this->starts || $blockadeStartsBeforeService >= $this->ends;
    }
}