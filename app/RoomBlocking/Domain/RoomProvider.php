<?php

namespace Source\RoomBlocking\Domain;

use Symfony\Component\Uid\Uuid;

interface RoomProvider
{
    public function findById(Uuid $roomId): Room;
}