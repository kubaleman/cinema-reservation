<?php

namespace Source\RoomBlocking\Domain;

interface BlockadeRepository
{
    public function save(RoomBlockade $blockade): void;
}