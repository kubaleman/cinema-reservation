<?php

namespace Source\RoomBlocking\Domain;

use DateTimeImmutable;
use Source\Shared\Domain\Minutes;
use Symfony\Component\Uid\Uuid;

class Room
{
    private function __construct(
        private readonly Uuid $roomId,
        private readonly Minutes $serviceTime,
        private readonly Blockades $blockades
    ) {
    }

    public static function of(Uuid $roomId, Minutes $serviceTime, Blockades $blockades): static
    {
        return new self($roomId, $serviceTime, $blockades);
    }

    /**
     * @throws CannotAddBlockadeException
     */
    public function newBlockade(DateTimeImmutable $blockadeStart, Minutes $blockadeTime): RoomBlockade
    {
        $blockadeEnds = $blockadeStart->add($blockadeTime->asTimeIntervalInMinutes());
        $timeIntervalToBlock = TimeInterval::of($blockadeStart, $blockadeEnds);

        $this->blockades->putBlockade($timeIntervalToBlock, $this->serviceTime);

        return new RoomBlockade(TimeInterval::of($blockadeStart, $blockadeEnds), $this->roomId);
    }
}