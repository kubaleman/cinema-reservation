<?php

namespace Source\RoomBlocking\Application;

use DateTimeImmutable;
use Source\RoomBlocking\Domain\BlockadeRepository;
use Source\RoomBlocking\Domain\CannotAddBlockadeException;
use Source\RoomBlocking\Domain\RoomProvider;
use Source\Shared\Domain\Minutes;
use Symfony\Component\Uid\Uuid;

class RoomBlockadeService
{
    public function __construct(
        private readonly RoomProvider $roomProvider,
        private readonly BlockadeRepository $blockadeRepository,
    ) {
    }

    public function blockRoom(Uuid $roomId, DateTimeImmutable $blockadeDateTime, Minutes $blockDuration): Uuid
    {
        $room = $this->roomProvider->findById($roomId);

        try {
            $blockade = $room->newBlockade($blockadeDateTime, $blockDuration);
            $this->blockadeRepository->save($blockade);
            return $blockade->roomBlockadeId;
        } catch (CannotAddBlockadeException $exception) {
            // todo thorw || result object
        }
    }
}