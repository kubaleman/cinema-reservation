<?php

namespace App\Controller;

use App\Service\ScreeningCreationDto;
use App\Service\ScreeningService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

class ScreeningController extends AbstractController
{
    public function __construct(
        private readonly ScreeningService $screeningService
    ) {
    }

    #[Route('/api/screening', name: 'api_screening', methods: ['POST'])]
    public function addScreeningInRoom(Request $request): JsonResponse
    {
        $content = json_decode($request->getContent(), true);

        $result = $this->screeningService->addNewScreeningInRoom(
            ScreeningCreationDto::ofArray($content)
        );

        if ($result) {
            return $this->json([
                'message' => 'Screening time added to room',
            ]);
        }

        return $this->json([
            'message' => 'Cannot add screening to this room at this time',
        ]);
    }
}
