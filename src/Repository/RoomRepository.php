<?php

namespace App\Repository;

use Ramsey\Collection\Collection;
use Source\RoomBlocking\Domain\Room;
use Source\RoomBlocking\Domain\RoomProvider;
use Source\RoomBlocking\Domain\TimeInterval;
use Source\Shared\Domain\Minutes;
use Symfony\Component\Uid\Uuid;

class RoomRepository implements RoomProvider
{
    private array $rooms = [];

    public function __construct()
    {
        $this->rooms['021a36f2-57b7-4a53-aec6-e0af39ac7c2e'] = Room::of(Minutes::ofInt(30), new Collection(TimeInterval::class, []));
    }

    public function findById(Uuid $roomId): Room
    {
        // todo database impl
        return $this->rooms[$roomId->toRfc4122()];
    }
}