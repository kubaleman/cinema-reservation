<?php

namespace App\Repository;


use App\Entity\Branch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Source\RoomBlocking\Domain\RoomBlockade;
use Source\RoomBlocking\Domain\TimeInterval;

/**
 * @extends ServiceEntityRepository<TimeInterval>
 *
 * @method TimeInterval|null find($id, $lockMode = null, $lockVersion = null)
 * @method TimeInterval|null findOneBy(array $criteria, array $orderBy = null)
 * @method TimeInterval[]    findAll()
 * @method TimeInterval[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlockadeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TimeInterval::class);
    }

    public function save(RoomBlockade $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    //    /**
    //     * @return Example[] Returns an array of Example objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('e')
    //            ->andWhere('e.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('e.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Example
    //    {
    //        return $this->createQueryBuilder('e')
    //            ->andWhere('e.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
