<?php

namespace App\Repository;

// proste repo, które wyciągnie mi dane dotyczące filmu z bazy
use JetBrains\PhpStorm\ArrayShape;
use Source\Shared\Domain\Minutes;
use Symfony\Component\Uid\Uuid;

class MovieCrudRepository
{
    #[ArrayShape([
        'minutesDuration' => Minutes::class
    ])]
    public function getDurationById(Uuid $movieId): Minutes
    {
        // typ array zostałby zmieniony na Dto lub DoctrineEntity
        // pobieramy z bazy tylko jedno pole
        return Minutes::ofInt(120);
    }
}