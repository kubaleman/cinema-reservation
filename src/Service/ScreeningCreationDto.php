<?php

namespace App\Service;

use DateTimeImmutable;
use Symfony\Component\Uid\Uuid;

class ScreeningCreationDto
{
    private function __construct(
        public readonly Uuid $roomId,
        public readonly Uuid $movieId,
        public readonly DateTimeImmutable $movieStartsTime,
    ) {
    }

    public static function ofArray(array $data): static
    {
        return new self(
            Uuid::fromString($data['roomId']),
            Uuid::fromString($data['movieId']),
            new DateTimeImmutable($data['movieStartsTime'])
        );
    }
}