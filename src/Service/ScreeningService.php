<?php

namespace App\Service;

use App\Entity\Screening;
use App\Repository\MovieCrudRepository;
use App\Repository\ScreeningRepository;
use Source\RoomBlocking\Application\RoomBlockadeService;

class ScreeningService
{
    public function __construct(
        private readonly RoomBlockadeService $roomBlockadeService,
        private readonly MovieCrudRepository $movieCrudRepository,
        private readonly ScreeningRepository $screeningRepository
    ) {
    }

    public function addNewScreeningInRoom(ScreeningCreationDto $screeningDto): bool
    {
        $roomId = $screeningDto->roomId;
        $movieId = $screeningDto->movieId;

        $movieDuration = $this->movieCrudRepository->getDurationById($movieId);

        //todo
        // obsłużyć przypadek kiedy blokowanie się nie powiedzie
        // propozycja obiekt rezultatu
        $blockadeId = $this->roomBlockadeService->blockRoom($roomId, $screeningDto->movieStartsTime, $movieDuration);
        $this->screeningRepository->save(new Screening($movieId, $blockadeId));

        return true;
    }
}