<?php

namespace App\Service;

use App\Repository\BlockadeRepository;
use App\Repository\RoomRepository;
use DateTimeImmutable;
use Source\RoomBlocking\Domain\CannotAddBlockadeException;
use Source\Shared\Domain\Minutes;
use Symfony\Component\Uid\Uuid;

class PartyService
{
    public function __construct(
        private readonly RoomRepository $roomRepository,
        private readonly BlockadeRepository $blockadeRepository
    ) {
    }

    // przykładowy serwis, jak można by wykorzystać obiekt Room i Blockade, ale w innym kontekście niż film
    public function addNewPartyInRoom(Uuid $roomId, DateTimeImmutable $partyDateTime): bool
    {
        $room = $this->roomRepository->findById($roomId);

        try {
            $blockade = $room->newBlockade($partyDateTime, Minutes::ofInt(3*60));
            $this->blockadeRepository->save($blockade);
        } catch (CannotAddBlockadeException $exception) {
            return false;
        }

        return true;
    }
}