<?php

namespace App\Entity;

use Symfony\Component\Uid\Uuid;

//crud potrzebny do trzymania połącenia miedzy filmem, a konkretną blokadą
class Screening
{
    private Uuid $id;

    public function __construct(
        public readonly Uuid $movieId,
        public readonly Uuid $roomBlockadeId
    ) {
        $this->id = Uuid::v4();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }
}
